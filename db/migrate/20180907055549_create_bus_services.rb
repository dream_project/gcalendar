class CreateBusServices < ActiveRecord::Migration[5.2]
  def change
    create_table :bus_services do |t|
      t.integer :user_id
      t.string :name
      t.integer :origin
      t.integer :destination
      t.date :journey_date
      t.string :event_id
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
