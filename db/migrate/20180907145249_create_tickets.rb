class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
    	t.string :pnr
    	t.string :booking_id
    	t.string :seat_number
    	t.integer	:seat_number_id
    	t.string	:first_name
    	t.string	:last_name
    	t.string	:email_id
    	t.string	:phone_number
      t.timestamps
    end
  end
end
