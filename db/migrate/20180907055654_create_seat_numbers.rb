class CreateSeatNumbers < ActiveRecord::Migration[5.2]
  def change
    create_table :seat_numbers do |t|
      t.integer :user_id
      t.string :seat_number
      t.integer :bus_service_id
      t.integer :status

      t.timestamps
    end
  end
end
