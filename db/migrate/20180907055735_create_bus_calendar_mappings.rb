class CreateBusCalendarMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :bus_calendar_mappings do |t|
      t.integer :user_id
      t.integer :bus_service_id
      t.string :google_calendar_id
      t.integer :status
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
