class CreateCalEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :cal_events do |t|
      t.integer :user_id
      t.integer :calendar_id
      t.string :event_id
      t.integer :status, default: 1
      t.date	:date
      t.integer :event_type
      t.string	:summary
      t.integer :created_by
      t.integer :updated_by
      t.string	:attendee
      t.timestamps
    end
  end
end
