class CreateHotelCalendarMappings < ActiveRecord::Migration[5.2]
  def change
    create_table :hotel_calendar_mappings do |t|
      t.integer :user_id
      t.integer :room_type_id
      t.integer :google_calendar_id
      t.integer :status

      t.timestamps
    end
  end
end
