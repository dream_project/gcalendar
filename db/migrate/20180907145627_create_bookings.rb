class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
    	t.date	:journey_date
    	t.string	:event_id
    	t.string	:service_id
    	t.string	:email_id
      t.timestamps
    end
  end
end
