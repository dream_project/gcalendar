class CreateRoomNumbers < ActiveRecord::Migration[5.2]
  def change
    create_table :room_numbers do |t|
      t.integer :user_id
      t.string :room_number
      t.integer :room_type_id
      t.integer :status

      t.timestamps
    end
  end
end
