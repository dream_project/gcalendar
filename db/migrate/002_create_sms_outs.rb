class CreateSmsOuts < ActiveRecord::Migration[5.2]
  def change
    create_table :sms_outs do |t|
	    t.integer  "user_id"
	    t.string   "message",                  :limit => 1024
	    t.integer  "body_message_length"
	    t.integer  "is_processed",             :limit => 1,    :default => AdminType::NO
	    t.integer  "status",                   :limit => 1,    :default => AdminType::NO
	    t.string   "message_transaction_num"
	    t.string   "gateway_provider_id",      :limit => 2
	    t.string   "mobile_number",            :limit => 16
	    t.string   "sender_number",            :limit => 16
	    t.integer  "total_length"
	    t.integer  "sms_count",                :limit => 1
	    t.string   "message_id"
	    t.string   "dlr_status",               :limit => 25
	    t.integer  "priority",                 :limit => 1,    :default => 1
	    t.datetime "deliver_on"
	    t.datetime "delivered_on"
	    t.string   "message_transaction_resp", :limit => 50
	    t.integer  "sms_pipe",                                 :default => 1
	    t.integer  "is_locked",                                :default => AdminType::NO
	    t.integer  "secure",                                   :default => AdminType::NO
	    t.datetime "created_at"
	    t.datetime "updated_at"
        t.timestamps
    end
  end
end
