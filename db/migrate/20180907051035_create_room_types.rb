class CreateRoomTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :room_types do |t|
      t.integer :user_id
      t.string :name
      t.integer :status

      t.timestamps
    end
  end
end
