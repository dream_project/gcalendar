class CreateHotelConfigurations < ActiveRecord::Migration[5.2]
  def change
    create_table :hotel_configurations do |t|
      t.integer :user_id 
      t.integer :configuration_type 
      t.integer :configuration_option 
      t.text :user_name 
      t.text :password 
      t.integer :status 
      t.text :sms_sender_label
      
      t.timestamps
    end
  end
end
