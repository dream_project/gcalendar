class CreateBoardingDropoffPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :boarding_dropoff_points do |t|
      t.integer :city_id
      t.string :name

      t.timestamps
    end
  end
end
