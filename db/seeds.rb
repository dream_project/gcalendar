# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

City.create([{ name: 'Chennai' }, { name: 'Bangalore' }, { name: 'Hyderabad' }, { name: 'Goa' }])

BoardingDropoffPoint.create([{ city_id: 1, name: 'Koyembedu' }, { city_id: 1, name: 'Egmore' }])
BoardingDropoffPoint.create([{ city_id: 2, name: 'Madivala' }, { city_id: 2, name: 'Silk Board' }])
BoardingDropoffPoint.create([{ city_id: 3, name: 'Kakinada' }, { city_id: 3, name: 'Kurnool' }])
BoardingDropoffPoint.create([{ city_id: 4, name: 'Porvorim' }, { city_id: 4, name: 'Canacona' }])

SeatNumber.create([{ user_id: 1, seat_number: '1A' }, { user_id: 1, seat_number: '1B' }, { user_id: 1, seat_number: '1C' }, { user_id: 1, seat_number: '1D' },{ user_id: 1, seat_number: '2A' }, { user_id: 1, seat_number: '2B' }, { user_id: 1, seat_number: '2C' }, { user_id: 1, seat_number: '2D' }, { user_id: 1, seat_number: '3A' }, { user_id: 1, seat_number: '3B' }, { user_id: 1, seat_number: '3C' }, { user_id: 1, seat_number: '3D' },{ user_id: 1, seat_number: '4A' }, { user_id: 1, seat_number: '4B' }, { user_id: 1, seat_number: '4C' }, { user_id: 1, seat_number: '4D' }, { user_id: 1, seat_number: '5A' }, { user_id: 1, seat_number: '5B' }, { user_id: 1, seat_number: '5C' }, { user_id: 1, seat_number: '5D' }, { user_id: 1, seat_number: '6A' }, { user_id: 1, seat_number: '6B' }, { user_id: 1, seat_number: '6C' }, { user_id: 1, seat_number: '6D' }, { user_id: 1, seat_number: '7A' }, { user_id: 1, seat_number: '7B' }, { user_id: 1, seat_number: '7C' }, { user_id: 1, seat_number: '7D' }])