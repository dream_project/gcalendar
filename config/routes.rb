Rails.application.routes.draw do
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :seat_numbers
  resources :bus_services do 
    collection do
      get 'sync_calendar'
      get 'service_status'
    end
  end

  resources :bookings do 
    collection do
      post 'bus_booking'
      match ':action(/:id(.:format))', via: [:get, :post]
    end
  end
  # resources :home
  resources :home do
    collection do
      # get "google_auth"
      # post "google_auth2"
      match ':action(/:id(.:format))', via: [:get, :post]
    end
  end
  resources :room_numbers
  resources :room_types
  resources :hotels do
    collection do
      get 'index'
      get 'google_calendar'
      post 'create_google_calendar'
    end
  end
  resources :buses do
    collection do
      get 'index'
      get 'google_calendar'
      get 'buses_calendar_mapping'
      post 'save_bus_calendar_mapping'
      post 'create_google_calendar'
      get 'search_bus'
      get 'search_service_result'
    end
  end

  root to: "buses#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end