class SmsPipe
  attr_accessor :id, :pipe_name, :pipe_type, :http_url, :https_url

  def SmsPipe.get_sms_pipes
    AdminType::SMS_PIPES_HASH.values.collect{|pipe| [pipe.pipe_name, pipe.pipe_type]}
  end
  
  def self.get_sms_pipe(pipe_type)
    return AdminType::SMS_PIPES_HASH[self.get_p1_pipe] if self.is_use_p1_pipe?
    AdminType::SMS_PIPES_HASH[pipe_type] || AdminType::SMS_PIPES_HASH[self.get_default_pipe]
  end
  
  def self.get_p1_pipe
    UNICEL_P1_PIPE
  end

  def self.get_default_pipe
    UNICEL_DEFAULT_PIPE # 
  end
  
  def self.is_use_p1_pipe?
    # Settings.USE_P1_PIPE_AS_DEFAULT == true  # set this flag Settings.USE_P1_PIPE_AS_DEFAULT to true or false using an SMS command and also 
  end
  
  # SmsPipe.get_url_str(pipe_type, send_number, dest_number, message, priority, append_str)
  def self.get_url_str(pipe_type, send_number, dest_number, message, priority, append_str, is_https = AdminType::NO)
      if is_https == AdminType::YES
        format(self.get_sms_pipe(pipe_type).https_url, send_number, dest_number, message, priority, append_str)
      else
        format(self.get_sms_pipe(pipe_type).http_url, send_number, dest_number, message, priority, append_str)
      end
  end
  
  # SMS Pipes
  UNICEL_DEFAULT_PIPE = 1
  UNICEL_P1_PIPE = 2
  # TEST URLS
  # DEF: http://www.unicel.in/SendSMS/sendmsg.php?uname=Mobee&pass=bee321&send=919845498454&dest=919243420313&msg=DEF-TEST&prty=1&concat=0 
  # P1:  http://www.unicel.in/SendSMS/sendmsg.php?uname=mobeeP1&pass=1BE2WD&send=919845498454&dest=919243420313&msg=P1-TEST&prty=1&concat=0 
  def self.init_sms_pipes_hash
    sp_hash = Hash.new
    sp_hash[UNICEL_DEFAULT_PIPE] = self.create_sms_pipe(UNICEL_DEFAULT_PIPE, "Unicel Default", "http://www.unicel.in/SendSMS/sendmsg.php?uname=Mobee&pass=bee321&send=%s&dest=%s&msg=%s&prty=%s%s", "http://www.unicel.in/SendSMS/sendmsg.php?uname=Mobee&pass=bee321&send=%s&dest=%s&msg=%s&prty=%s%s")
    # sp_hash[UNICEL_P1_PIPE] = self.create_sms_pipe(UNICEL_P1_PIPE, "Unicel P1 Pipe", "http://www.unicel.in/SendSMS/sendmsg.php?uname=mobeed&pass=1BC2WQ&send=%s&dest=%s&msg=%s&prty=%s%s") # DEMO
    sp_hash[UNICEL_P1_PIPE] = self.create_sms_pipe(UNICEL_P1_PIPE, "Unicel P1 Pipe", "http://www.unicel.in/SendSMS/sendmsg.php?uname=mobeeP1&pass=1BE2WD&send=%s&dest=%s&msg=%s&prty=%s%s","https://www.unicel.in/SendSMS/sendmsg.php?uname=mobeeP1&pass=1BE2WD&send=%s&dest=%s&msg=%s&prty=%s%s")
    sp_hash
  end

  def self.create_sms_pipe(pipe_type, pipe_name, http_url, https_url)
    sms_pipe = SmsPipe.new
    sms_pipe.pipe_type = pipe_type
    sms_pipe.pipe_name = pipe_name
    sms_pipe.http_url = http_url
    sms_pipe.https_url = https_url
    sms_pipe
  end
end