class City < ApplicationRecord
	def City.get_cities_array
		cities = City.all
		@cities_array = []
    cities.each {|city|
      @cities_array << [city.name, city.id]
    }
    return @cities_array
	end

	def City.get_cities_hash
    cities = City.all
		@cities_hash = []
    cities.each {|city|
      @cities_hash[city.id] = city.name
    }
    return @cities_hash
	end
end
