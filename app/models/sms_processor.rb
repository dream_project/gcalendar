BATCH_SIZE = 100
# require 'rest-client'

module SmsProcessor
  
  def SmsProcessor.process_sms_out(sms_out_id)
    sms_out = SmsOut.where(["id = ?", sms_out_id]).first
    return if sms_out.blank?
    sms_sender_label = "BITLAS"
    sms_out.update_attribute("is_locked", AdminType::YES)
    gate_way, username, password = HotelConfiguration.get_sms_gateway_user_name_and_password(sms_out.user_id) #[AdminType::SMS_ENGINE_VFIRST_XML, "asdasd", "asd" ] #[AdminType::SMS_ENGINE_VFIRST_XML, "bitlasoftware", "warbitof" ]
    sms_gateway = VeltiSmsEngineHTTP.new if gate_way == AdminType::SMS_VELTI_INDIA
    sms_gateway = VFirstXMLSMSEngine.new if gate_way == AdminType::SMS_ENGINE_VFIRST_XML
    sms_gateway.send_out([sms_out.id], false, username, password , sms_sender_label)
    sms_out.update_attribute("is_locked", AdminType::NO)
  end  
end

class SMSEngineFactory
  include Singleton

  private_class_method :new
  @@unicel_engine = nil
  @@unicel_engine_bulk = nil
  @@unicel_engine_smpp = nil
  @@vfirst_engine_xml = nil
  @@kannel_engine = nil
  @@vfirst_engine_http = nil
  @@routesms_engine_http = nil
  @@sms_gupshup_engine_http = nil
  @@velti_sms_engine_http = nil
  @@flashsms_engine_http = nil

  
  def SMSEngineFactory.get_engine(engine = AdminType::SMS_ENGINE_DEFAULT)
    fact = SMSEngineFactory.instance
    return @@kannel_engine if engine == AdminType::SMS_ENGINE_KANNEL
    return @@unicel_engine if engine == AdminType::SMS_ENGINE_UNICEL_HTTP
    return @@unicel_engine_bulk if engine == AdminType::SMS_ENGINE_UNICEL_HTTP_BULK
    return @@unicel_engine_smpp if engine == AdminType::SMS_ENGINE_UNICEL_SMPP
    return @@vfirst_engine_xml if engine == AdminType::SMS_ENGINE_VFIRST_XML
    return @@vfirst_engine_http if engine == AdminType::SMS_ENGINE_VFIRST_HTTP
    return @@routesms_engine_http if engine == AdminType::SMS_ENGINE_ROUTESMS_HTTP
    return @@sms_gupshup_engine_http if engine == AdminType::SMS_GUPSHUP_XML
    return @@velti_sms_engine_http if engine == AdminType::SMS_VELTI_INDIA
    return @@flashsms_engine_http if engine == AdminType::SMS_FLASH_SMS_NIGERIA
  end  
  
  def initialize()
    if (@@unicel_engine.nil?)
      @@kannel_engine = KannelSMSEngine.new
      @@unicel_engine = UniCelSMSEngine.new
      @@unicel_engine_smpp = nil
      @@unicel_engine_bulk = nil
      @@vfirst_engine_xml = VFirstXMLSMSEngine.new 
      @@vfirst_engine_http = VFirstHttpSMSEngine.new 
      @@routesms_engine_http = RouteSMSEngineHTTP.new
      @@flashsms_engine_http = FlashSMSEngineHTTP.new
      @@sms_gupshup_engine_http =  SmsGupShupEngineHTTP.new
      @@velti_sms_engine_http = VeltiSmsEngineHTTP.new
    end
  end
end

class SMSAbstractEngine
  def send_out(sms_outs, sms_type, use_mobee = false, user_name = nil, password = nil,gsm_label)    
  end

  def test    
    puts "SMSAbstractEngine"
  end
  
  protected
  SENDER_GSM_MOBEE = "mobee.in"  
  SENDER_GSM = "919008666233" #"91900TOMOBEE"
  SENDER_CDMA = "919008666233"
  
  CDMA_NUMBERS = %w(9192 9193)
  def get_mobee_number(send_to_num, use_mobee = false)
    short_num = send_to_num.slice(0, 4)
    if (CDMA_NUMBERS.include? short_num)
      SENDER_CDMA
    else
      (use_mobee == true)? SENDER_GSM_MOBEE : SENDER_GSM
    end
  end
end

UNICEL_USERNAME = "Mobee"
UNICEL_PWD = "bee321"

#UNICEL_SENDER = "91900TOMOBEE"
#UNICEL_SENDER_CDMA = "919008666233"
UNICEL_SMS_OUT_URL = "http://www.unicel.in/SendSMS/sendmsg.php?uname=#{UNICEL_USERNAME}&pass=#{UNICEL_PWD}&send=%s&dest=%s&msg=%s%s&prty=%s"
CONCAT_APPEND_STR = "&concat=1"  # Not using CONCAT Feature - Simple!


SENDER_NUMBER_NEW = "HtlSimply" #AdminType::PRODUCT_NAME


KANNEL_USERNAME = "moBKanBits"
KANNEL_PWD = "moB301Kan"
#KANNEL_SENDER = "mobee.in"
#KANNEL_SMS_OUT_URL = "http://localhost:13713/cgi-bin/sendsms?username=#{KANNEL_USERNAME}&password=#{KANNEL_PWD}&from=#{KANNEL_SENDER}&to=%s&text=%s%s&priority=%s"
KANNEL_SMS_OUT_URL = "http://localhost:13713/cgi-bin/sendsms?username=#{KANNEL_USERNAME}&password=#{KANNEL_PWD}&from=%s&to=%s&text=%s%s&priority=%s"
#CONCAT_APPEND_STR = ""
# require 'xmlsimple'
class VFirstXMLSMSEngine < SMSAbstractEngine

  @@last_crashed_time = nil
  USERNAME =   "bitlasoftware"  #"demobitlasoft"
  PASSWORD =  "warbitof" #"bitla98api"
  SMS_SERVICE_URL = "http://api.myvaluefirst.com/psms/servlet/psms.Eservice2";
  ADDRESS_TAG = '<ADDRESS FROM="%s" TO="%s" SEQ="%s" TAG="%s"/>';
  DATA_TAG = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1/psms/dtd/messagev12.dtd" ><MESSAGE VER="1.2"><USER USERNAME="%s" PASSWORD="%s"/><SMS UDH="0" CODING="1" TEXT="%s" PROPERTY="0" ID="1">%s</SMS></MESSAGE>'
    
  def send_out(sms_outs, use_label= false, username = nil, password = nil,gsm_label)
   
    return if sms_outs.blank?

    sms_outs.each {|sms_id|
      
      break unless is_process_sms?
      sms = SmsOut.find(sms_id)
      next if sms.is_processed == AdminType::YES

      message = sms.message.gsub("&", "%26amp;") if !sms.message.blank?
      dataParameter = get_data_parameter(SENDER_NUMBER_NEW, sms.mobile_number,message);  
      p "dataParameter=#{dataParameter}"
      resp = SimpleHttp.post SMS_SERVICE_URL, "data=#{CGI::escape(dataParameter)}&action=send"
      resp_parsed = XmlSimple.xml_in resp

      error = nil
       guid = nil
       pp resp_parsed
      if AdminType::IS_LOCAL == true
        guid = "LOCAL ENV"
      else
        if !resp_parsed['GUID'].blank?
         guid = resp_parsed['GUID'][0]['GUID'] 
         error = resp_parsed['GUID'][0]['ERROR']
       else
         error_hash = resp_parsed['Err']
         error  = error_hash[0]['Desc'].to_a
        end
        error = error.join(',') unless error.nil?
      end
    
      if error.blank?
        is_processed = AdminType::YES
        sms.status = AdminType::YES # Is delivered Status
        sms.message_transaction_num = guid   
        sms.message_transaction_resp  = nil
      else  
        is_processed = sms.delivered_on.blank? ? AdminType::NO : AdminType::YES
        sms.status = AdminType::NO 
        sms.message_transaction_num = guid
        sms.message_transaction_resp  = error
      end
      sms.is_processed = is_processed
      sms.delivered_on = Time.now #CommonUtils.get_current_utc_time
      sms.save!
      raise "SMS Failed" unless error.blank?
    }
  end
  
  def encode_HTML(s)
      out = ""
      s.each_byte{|c|
        if ((c >= 32 && c<=46) || (c >= 58 && c<=64) || (c >= 91 && c<=96) || (c >= 123 && c<=126))
          out += "&##{c};"
        else
          out += c.chr
        end
      }
    #  puts "encode_HTML s=#{s}, out=#{out}"
      return out
  end
  
  def test
    se = VFirstXMLSMSEngine.new
    toList = ["destination1,destination2,xxxxxx"]
    se.send_SMS("<Enter Senders' Number>", toList, SMSEngine.encode_HTML("hello <ttes> world"))
  end
  
  def is_process_sms?
    ret_val = (@@last_crashed_time.nil?) || (!@@last_crashed_time.nil? && (CommonUtils.get_current_time - @@last_crashed_time) > 300)
    @@last_crashed_time = nil if ret_val
    ret_val
  end
  
  private  
  # TODO SEt TAG
  def get_data_parameter(from, toList, message, tag = "") 
    userName =  USERNAME
    password = PASSWORD
    addressTags = ""
    i = 1;
    # toList.each {|li|
      address = toList.to_s
      addressTags = format(ADDRESS_TAG, from, address, i, tag)
    #   i += 1
    # }
    return format(DATA_TAG, userName, password, message, addressTags)
  end
end

# Sms Responses
#Sent.                                                        Message sent Successfully 
#Sent. Split into N                                           Message was sent, however it was found to be longer than permitted limit and hence was spitted into multiple messages 
#Number(s) has/have been denied by white- and/or black-lists. Invalid Recipient numbers. 
#Empty receiver number not allowed, rejected                  Recipient number is empty. 
#Sender missing and no global set, rejected                   Sender number is missing. 
#Empty text not allowed, rejected.                            Message Text is empty. 
#unknown request                                              Kannel is down or SMSC connectivity problem. 
#Authorization failed for sendsms                             Invalid Username and Password. 


#http://www.myvaluefirst.com/smpp/sendsms?username=demobitla&password=bitlasoft10&to=9986733248&from=MBitsy&udh=&text=testmessage

#velti credentials  515524/bte@1
class VeltiSmsEngineHTTP < SMSAbstractEngine
  
  SMS_OUT_URL = "luna.a2wi.co.in:7501/failsafe/HttpLink?"
  #its open sender id  for Velti now, we can configure any name  SENDER_NUMBER as 6 characters
  
  @@last_crashed_time = nil;
  
  def send_out(sms_outs, use_mobee = false, username = nil, password=nil, gsm_label)
    sms_outs.each {|sms|
      break unless is_process_sms?
      sms = SmsOut.find(sms)
      next if sms.is_processed == AdminType::YES
      mobile_number  = sms.mobile_number.strip
      mobile_number  = mobile_number.gsub(" ", "")
      mobile_number  = mobile_number.gsub("\r\n", "")
      gsm_label = "HSMPLY" if gsm_label.blank?
      url_str = get_url(gsm_label, mobile_number, CGI.escape(sms.message.gsub("\n", '')), username, password)
      begin
        # sms.attempts = sms.attempts + 1
        sms.save!
        trans_num = (AdminType::IS_LOCAL == true) ? "LOCAL ENV" : SimpleHttp.get(url_str) 
         resp = get_transaction_response(trans_num)
         status = resp[:status]
        if status
          is_processed = AdminType::YES
          sms.status = AdminType::YES # Is delivered Status
          sms.delivered_on = Date.today
          sms.message_transaction_num = resp[:transaction_number]
        else
          is_processed = sms.delivered_on.blank? ? AdminType::NO : AdminType::YES
          sms.status = AdminType::NO # Is delivered Status
           sms.sms_error = resp[:info] 
          sms.message_transaction_resp = trans_num
        end
        sms.is_processed = is_processed
        sms.delivered_on = Date.today
        sms.save!
      rescue Exception => detail
        @@last_crashed_time = Date.today
        BitlaErrorNotifier.notify_alert("#{I18n.t 'common_m.velti_sms_out_error'} - #{@@last_crashed_time} -- #{detail}","#{detail.backtrace.join("\n")}")
      end  
    }
  end  
  
    # process if there is no crash or if its crashed 5 min ago - applies across multiple threads
  def is_process_sms?
    ret_val = (@@last_crashed_time.nil?) || (!@@last_crashed_time.nil? && (CommonUtils.get_current_time - @@last_crashed_time) > 300)
    @@last_crashed_time = nil if ret_val
    ret_val
  end
  
  private
  
  def get_url(source, destination, message, username, password)
    return "#{SMS_OUT_URL}aid=#{username}&pin=#{password}&mnumber=#{destination}&message=#{message}&signature=#{source}"
  end

  def get_transaction_response(response)
    resp_hash = Hash.new
    trans = response.split("&")
    req_str = trans[0].split("~")
    sub_string = req_str[0].split("=")
    s_str = sub_string[0].split(" ")
    status = s_str[1] == "Accepted" ? true : false
    resp_hash[:status] = status
    resp_hash[:transaction_number] = sub_string[1]  if status
    info_string = trans[1].split("=")
    resp_hash[:info] = info_string[1]
    resp_hash[:trans_number] = response
    resp_hash  
  end

end
