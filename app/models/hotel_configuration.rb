class HotelConfiguration < ApplicationRecord
  
  belongs_to :users

  def name
    
  end
  
  def is_pg_configuration_type?
    self.configuration_type == AdminType::PAYMENT_GATE_CONFIGURATION_TYPES
  end

  def is_sms_configuration_type?
    self.configuration_type == AdminType::SMS_CONFIGURAION_TYPES
  end
  
  def  HotelConfiguration.get_sms_gateway_user_name_and_password(user_id)
    #for hotel setup hotel_configuration is we are giving directly velti configuration
    return [AdminType::SMS_VELTI_INDIA, "515524", "bte@1" ] if user_id == 1
    hc = HotelConfiguration.get_pg_gateway_details(user_id,AdminType::SMS_CONFIGURAION_TYPES)
    hc.blank? ? [nil, nil, nil] : [hc.configuration_option, hc.user_name, hc.password]
  end
  
  def  HotelConfiguration.get_pg_gateway_user_name_and_password(user_id)
    hc = HotelConfiguration.get_pg_gateway_details(user_id, AdminType::PAYMENT_GATE_CONFIGURATION_TYPES)
    hc.blank? ? [nil,nil, nil] : [hc.configuration_option, hc.user_name, hc.password, hc.merchant_key]
  end
  def  HotelConfiguration.get_pg_gateway_details(user_id,configuration_type)
  	HotelConfiguration.where(["user_id=? and configuration_type=?",user_id,configuration_type]).first 
  end

  def HotelConfiguration.get_account_wise_configuration_hash(user_ids, selected_cols = '*')
    hotel_config_hash = Hash.new
    HotelConfiguration.where(["user_id in (?) and status =?", user_ids, HotelConstant::YES]).select(selected_cols).each {|hc| hotel_config_hash[hc.user_id] = hc}
    hotel_config_hash
  end

  def is_global_pay?
    self.configuration_option == AdminType::PG_GLOBAL_PAY
  end

  def is_hs_pay_gay?
    self.configuration_option == AdminType::HS_PAY_GAY
  end

  def self.get_all_configuration_types(configuration_option)
    HotelConfiguration.where("configuration_option = ?  and configuration_type = ? ",configuration_option, AdminType::PAYMENT_GATE_CONFIGURATION_TYPES)
  end

  def self.get_sms_configuration_type_hotels
    # account_ids = Account.where("account_status = ?",HotelConstant::ACTIVE_ACCOUNT).pluck("id")
    # HotelConfiguration.where(["user_id in (?) and configuration_type = ? and password is not null and password != ? and user_name != ? and  user_name is not null",  account_ids,AdminType::SMS_CONFIGURAION_TYPES,'',''])
  end

end
