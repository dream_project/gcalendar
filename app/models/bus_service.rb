class BusService < ApplicationRecord
	def BusService.get_bus_services_array(user_id)
		@bus_services = BusService.where("user_id=?", user_id)
    @bus_services_array = []
    @bus_services.each {|bus_service|
      @bus_services_array << [bus_service.name, bus_service.id]
    }
    return @bus_services_array
	end
end
