class SmsOut < ApplicationRecord
  
  def is_processed?
    self.is_processed.to_i == AdminType::YES
  end
  
  def is_concat_required? 
    self.message.length > 160
  end

  def self.send_sms(user_id, mobile_number, message)
    user_id = 1
    length = 4
    count = 2
    sms_out = SmsOut.create!(:user_id => user_id, :message => message, :body_message_length => length, :mobile_number => mobile_number, :sms_count => count, :total_length => length, :is_locked => AdminType::NO, :sms_pipe => 2, :priority => 1)
    SmsProcessor.process_sms_out(sms_out.id)
  end

end
