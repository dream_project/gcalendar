class ApplicationMailer < ActionMailer::Base
  default from: 'sssaravanan.ece@gmail.com'
  layout 'mailer'

  def sample_email
    @user = "saravanan.selvan27@gmail.com"
    mail(to: @user, subject: 'Sample Email')
  end
end
