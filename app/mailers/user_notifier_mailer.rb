class UserNotifierMailer < ApplicationMailer
	def book_email(mail_id, subject, url)
    @recipients = mail_id
    @from = "GCalendar"
    @subject = subject
    @sent_on = Time.now
    @url = url
    mail(:from => @from, :to => @recipients , :subject => @subject, :date => @sent_on)
  end
end
