class GCal
require 'google/apis/calendar_v3'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'


OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'.freeze
APPLICATION_NAME = 'Google Calendar API Ruby Quickstart'.freeze
CREDENTIALS_PATH = "#{Rails.root}/public/credentials.json".freeze
TOKEN_PATH = 'token.yaml'.freeze
SCOPE = Google::Apis::CalendarV3::AUTH_CALENDAR

  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization. If authorization is required,
  # the user's default browser will be launched to approve the request.
  #
  # @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials

  def initialize(code)
    @code = code
  end

  def create_new_event(date, calendar_id)
    service = Google::Apis::CalendarV3::CalendarService.new
    service.client_options.application_name = APPLICATION_NAME
    service.authorization = authorize
    event = Google::Apis::CalendarV3::Event.new(
      summary: 'Cancelled',
      location: '',
      description: '',
      start: {
        date: date,
        time_zone: 'Asia/Calcutta',
      },
      end: {
        date: date,
        time_zone: 'Asia/Calcutta',
      },
    )
    result = service.insert_event(calendar_id, event)
    return result
  end

  def delete_event(event_id, calendar_id)
    service = Google::Apis::CalendarV3::CalendarService.new
    service.client_options.application_name = APPLICATION_NAME
    service.authorization = authorize
    result = service.delete_event(calendar_id, event_id)
  end



  def authorize
    client_id = Google::Auth::ClientId.from_file(CREDENTIALS_PATH)
    token_store = Google::Auth::Stores::FileTokenStore.new(file: TOKEN_PATH)
    authorizer = Google::Auth::UserAuthorizer.new(client_id, SCOPE, token_store)
    user_id = 'default'
    credentials = authorizer.get_credentials(user_id)
    return credentials if credentials.present?
    if @code.nil?
      url = authorizer.get_authorization_url(base_url: OOB_URI)
      puts 'Open the following URL in the browser and enter the ' \
           "resulting code after authorization:\n" + url
      return url
    else
      begin
        credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: @code, base_url: OOB_URI
        )
        credentials
      rescue Signet::AuthorizationError => ex
        response = ex.message
        [response,304]
      end
    end
  end

  def calendar_list
    service = Google::Apis::CalendarV3::CalendarService.new
    service.client_options.application_name = APPLICATION_NAME
    service.authorization = authorize
    calendar_id = 'primary'
    response = service.list_calendar_lists
    calendar_items = response.items
    calendar_ids = []
    calendar_items.each do |item|
      calendar_ids << item.id
    end
    # list_events
  end

  def create_new_calendar(name)
    service = Google::Apis::CalendarV3::CalendarService.new
    service.client_options.application_name = APPLICATION_NAME
    service.authorization = authorize
    calendar = Google::Apis::CalendarV3::Calendar.new(
    summary: "#{name}",
    time_zone: 'Asia/Calcutta'
    )
    result = service.insert_calendar(calendar)
  end

  # def create_new_event
  #   service = Google::Apis::CalendarV3::CalendarService.new
  #   service.client_options.application_name = APPLICATION_NAME
  #   service.authorization = authorize
  #   event = Google::Apis::CalendarV3::Event.new(
  #     summary: 'Blocked',
  #     location: '800 Howard St., San Francisco, CA 94103',
  #     description: 'A chance to hear more about Google\'s developer products.',
  #     start: {
  #       date: '2018-09-08',
  #       time_zone: 'Asia/Calcutta',
  #     },
  #     end: {
  #       date: '2018-09-09',
  #       time_zone: 'Asia/Calcutta',
  #     },
  #   )
  #   result = service.insert_event('jrpmauf71o8fslll14ipunoask@group.calendar.google.com', event)
  #   puts "Event created: #{result.html_link}"
  # end

  def create_event_webhook
    service = Google::Apis::CalendarV3::CalendarService.new
    service.client_options.application_name = APPLICATION_NAME
    service.authorization = authorize
    channel = Google::Apis::CalendarV3::Channel.new(address: "https://283033fa.ngrok.io/", id: "primary", type: "web_hook")
    webhook = service.watch_event('primary', channel, single_events: true, time_min: Time.now.iso8601)
  end

  def list_events(cal_id, user_id)
    service = Google::Apis::CalendarV3::CalendarService.new
    service.client_options.application_name = APPLICATION_NAME
    service.authorization = authorize
    calendar_id = cal_id
    response = service.list_events(calendar_id,
                                    single_events: true,
                                    order_by: 'startTime',
                                    time_min: Time.now.iso8601,
                                    time_zone: 'Asia/Calcutta'


    )

    event_ids = []
    response.items.each do |event|
      next if event.start.date.nil? || event.end.date.nil?
      if event.attendees.blank?
        # next if event.created.to_date < Date.today
        event_ids << {id: event.id, start: event.start.date.to_date, end: event.end.date.to_date, summary: event.summary, type: 1}
      else
        event_ids << {id: event.id, start: event.start.date.to_date, end: event.end.date.to_date, summary: event.summary, type: 2, email: event.attendees.first.email}
      end
    end
    events = []
    event_ids.each do |event|
      (event[:start]..event[:end]-1).each do |date|
        type = event[:type] == 1 ? 1 : 2
        if type == 1
        # bus_avail = CalEvent.where("calendar_id = ? and date = ? and event_type = ?",calendar_id,date, type).first
        #   if bus_avail.blank?
        #     eve = CalEvent.create!(user_id: user_id, calendar_id: calendar_id, event_id: event[:id], date: date, created_by: user_id, updated_by: user_id, summary: event[:summary], event_type: type)
        #   end
          bus_ser = BusCalendarMapping.where("google_calendar_id = ?",calendar_id).first
          if bus_ser.present?
          bus_avail = BusService.find_by_id(bus_ser.bus_service_id)
          # if bus_avail.blank?
          next if bus_avail.status == 2
          bus_avail.update_attributes(:status => 2, event_id: event[:id])
            # eve = CalEvent.create!(user_id: user_id, calendar_id: calendar_id, event_id: event[:id], date: date, created_by: user_id, updated_by: user_id, summary: event[:summary], event_type: type)
            eve = event[:id]
          end

          message = "Hi sir/madam Your Bus service cancelled"
          mobile_no = AdminType::SMS_ALERT_NUMBERS
          mobile_no.each do |phone_number|
            guest_message = "Hi Your Bus Service Cancelled"
            SmsOut.send_sms(user_id,phone_number ,guest_message)
            # SmsOut.send_sms(user_id,user.mobile_number ,message)
          end
        else
          bus_ser = BusCalendarMapping.where("google_calendar_id = ?",calendar_id).first
          book_avail = Booking.where("event_id =? ",event[:id]).first
          next if book_avail.present?
          if bus_ser.present?
            eve = CalEvent.create!(user_id: user_id, calendar_id: calendar_id, event_id: event[:id], date: date, created_by: user_id, updated_by: user_id, summary: event[:summary], event_type: type, attendee: event[:email])
            # bus_avail = BusService.find_by_id(bus_ser.bus_service_id)
            # eve = Booking.create!(journey_date: date, event_id: event[:id], service_id: calendar_id, email_id: event[:email])
            eve = event[:id]

            # ticket = Ticket.create!(booking_id: eve.id, email_id: event[:email])
            url = "http://localhost:3000//bookings/bus_booking/"+bus_ser.bus_service_id.to_s+"?date="+date.to_s+"&price=600"+"&calendar_id="+calendar_id.to_s+"&event_id="+eve.to_s
            UserNotifierMailer.book_email(event[:email], "SRS Travels: Book Your Journey For #{date}", url).deliver_now!
          end
        end
        events << eve
      end
    end
    events
  end

  

end