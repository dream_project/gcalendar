json.extract! seat_number, :id, :seat_number, :bus_service_id, :status, :created_at, :updated_at
json.url seat_number_url(seat_number, format: :json)
