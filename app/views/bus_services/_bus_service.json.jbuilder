json.extract! bus_service, :id, :name, :status, :created_at, :updated_at
json.url bus_service_url(bus_service, format: :json)
