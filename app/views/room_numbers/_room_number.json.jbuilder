json.extract! room_number, :id, :room_number, :room_type_id, :status, :created_at, :updated_at
json.url room_number_url(room_number, format: :json)
