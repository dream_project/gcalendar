class BusesController < ApplicationController
  before_action :authenticate_user!

  def create_google_calendar
    @gcal_code = current_user.gcal_code
    gcal = GCal.new(@gcal_code)
    gcal.create_new_calendar(params[:report][:name])
    @calendar_list = gcal.calendar_list
    flash[:notice]  = "Calendar Created Successfully"
  end

  def search_service_result
    @bus_services = BusService.where("user_id=? and journey_date=?",params[:user_id], params[:date].to_date)
    @cities = City.get_cities_hash
    render partial: "search_service_result"
  end

  protect_from_forgery
    
  def index
    # UserNotifierMailer.sample_email.deliver_now
  end
  
  def google_calendar
    @gcal_code = current_user.gcal_code
    gcal = GCal.new(@gcal_code)
    @url = gcal.authorize
    if @url.class == Google::Auth::UserRefreshCredentials
      @calendar_list = gcal.calendar_list
    end
  end

  def buses_calendar_mapping
    @gcal_code = current_user.gcal_code
    gcal = GCal.new(@gcal_code)
    @url = gcal.authorize
    if @url.class == Google::Auth::UserRefreshCredentials
      @calendar_list = gcal.calendar_list
      @services = BusService.get_bus_services_array(current_user.id)
      @calendar_array = []
        @calendar_list.each {|calendar|
        @calendar_array << [calendar.summary, calendar.id]
      }
      @mappings = BusCalendarMapping.where("user_id=?", current_user.id)
      @mappings_hash = {}
      @mappings.each {|mapping|
        @mappings_hash[mapping.bus_service_id] = mapping.google_calendar_id
      }
    end
  end

  def create_google_calendar
    @gcal_code = current_user.gcal_code
    gcal = GCal.new(@gcal_code)
    gcal.create_new_calendar(params[:report][:name])
    @calendar_list = gcal.calendar_list
    @room_types = BusService.get_bus_services_array(current_user.id)
    flash[:notice]  = "Calendar Created Successfully"
  end

  def save_bus_calendar_mapping
    val_goo_cal_map = BusCalendarMapping.where("user_id=? and bus_service_id!=? and google_calendar_id=?", params[:user_id], params[:service_id], params[:cal_id]).first
    if val_goo_cal_map.blank?
      goo_cal_map = BusCalendarMapping.where("user_id=? and bus_service_id=?", params[:user_id], params[:service_id]).first
      goo_cal_map = goo_cal_map.blank? ? BusCalendarMapping.new : goo_cal_map
      goo_cal_map.user_id = params[:user_id]
      goo_cal_map.bus_service_id = params[:service_id]
      goo_cal_map.google_calendar_id = params[:cal_id]
      goo_cal_map.created_by = params[:user_id]
      goo_cal_map.updated_by = params[:user_id]
      goo_cal_map.save!
      render plain: 1
    else
      render plain: 2
    end

  end

  def search_bus
    
  end
   
end
