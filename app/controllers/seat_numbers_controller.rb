class SeatNumbersController < ApplicationController
  before_action :set_seat_number, only: [:show, :edit, :update, :destroy]

  # GET /seat_numbers
  # GET /seat_numbers.json
  def index
    @seat_numbers = SeatNumber.where("user_id=?", current_user.id)
    @bus_services = BusService.all
    @bus_services_hash = {}
    @bus_services.each {|bus_service|
      @bus_services_hash[bus_service.id] = bus_service.name
    }
  end

  # GET /seat_numbers/1
  # GET /seat_numbers/1.json
  def show
  end

  # GET /seat_numbers/new
  def new
    @seat_number = SeatNumber.new
    @bus_services = BusService.where("user_id=?", current_user.id)
    @bus_services_array = []
    @bus_services.each {|bus_service|
      @bus_services_array << [bus_service.name, bus_service.id]
    }
  end

  # GET /seat_numbers/1/edit
  def edit
    @bus_services = BusService.where("user_id=?", current_user.id)
    @bus_services_array = []
    @bus_services.each {|bus_service|
      @bus_services_array << [bus_service.name, bus_service.id]
    }
  end

  # POST /seat_numbers
  # POST /seat_numbers.json
  def create
    @seat_number = SeatNumber.new(seat_number_params)

    respond_to do |format|
      if @seat_number.save
        format.html { redirect_to @seat_number, notice: 'Seat number was successfully created.' }
        format.json { render :show, status: :created, location: @seat_number }
      else
        format.html { render :new }
        format.json { render json: @seat_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seat_numbers/1
  # PATCH/PUT /seat_numbers/1.json
  def update
    respond_to do |format|
      if @seat_number.update(seat_number_params)
        format.html { redirect_to @seat_number, notice: 'Seat number was successfully updated.' }
        format.json { render :show, status: :ok, location: @seat_number }
      else
        format.html { render :edit }
        format.json { render json: @seat_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seat_numbers/1
  # DELETE /seat_numbers/1.json
  def destroy
    @seat_number.destroy
    respond_to do |format|
      format.html { redirect_to seat_numbers_url, notice: 'Seat number was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seat_number
      @seat_number = SeatNumber.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seat_number_params
      params.require(:seat_number).permit(:seat_number, :bus_service_id, :status, :user_id)
    end
end
