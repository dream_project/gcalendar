class RoomNumbersController < ApplicationController
  before_action :set_room_number, only: [:show, :edit, :update, :destroy]

  # GET /room_numbers
  # GET /room_numbers.json
  def index
    @room_numbers = RoomNumber.all
    @room_types = RoomType.all
    @room_types_hash = {}
    @room_types.each {|room_type|
      @room_types_hash[room_type.id] = room_type.name
    }
  end

  # GET /room_numbers/1
  # GET /room_numbers/1.json
  def show
  end

  # GET /room_numbers/new
  def new
    @room_number = RoomNumber.new
    @room_types = RoomType.all
    @room_types_array = []
    @room_types.each {|room_type|
      @room_types_array << [room_type.name, room_type.id]
    }
  end

  # GET /room_numbers/1/edit
  def edit
  end

  # POST /room_numbers
  # POST /room_numbers.json
  def create
    @room_number = RoomNumber.new(room_number_params)

    respond_to do |format|
      if @room_number.save
        format.html { redirect_to @room_number, notice: 'Room number was successfully created.' }
        format.json { render :show, status: :created, location: @room_number }
      else
        format.html { render :new }
        format.json { render json: @room_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /room_numbers/1
  # PATCH/PUT /room_numbers/1.json
  def update
    respond_to do |format|
      if @room_number.update(room_number_params)
        format.html { redirect_to @room_number, notice: 'Room number was successfully updated.' }
        format.json { render :show, status: :ok, location: @room_number }
      else
        format.html { render :edit }
        format.json { render json: @room_number.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /room_numbers/1
  # DELETE /room_numbers/1.json
  def destroy
    @room_number.destroy
    respond_to do |format|
      format.html { redirect_to room_numbers_url, notice: 'Room number was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room_number
      @room_number = RoomNumber.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_number_params
      params.require(:room_number).permit(:room_number, :room_type_id, :status, :user_id)
    end
end
