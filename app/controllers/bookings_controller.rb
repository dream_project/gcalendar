class BookingsController < ApplicationController
	protect_from_forgery
	def bus_booking
		book_avail = Booking.where("event_id =? ",params[:event_id]).first
		if book_avail.blank?
			@tickets = []
			bookings = Booking.where("journey_date = ? and service_id =?",params[:date],params[:id])
			bookings.each do |booking_id|
				ticket = Ticket.where("booking_id = ?",booking_id).first
				next if ticket.blank?
				@tickets << ticket.seat_number
			end
			render :layout => false
		else
			render :partial => "booking_expired"
		end
	end

	def temp_reserve
		event = CalEvent.find_by_event_id(params[:event_id])
		if book_avail.blank?
			bus_map = BusCalendarMapping.where("google_calendar_id = ?",params[:calendar_id]).first
			service_id = bus_map.bus_service_id
			byebug
			booking = Booking.create!(journey_date: params[:date], event_id: params[:event_id], service_id: service_id, email_id: event[:attendee])
			seats = params[:seats]
			from = params[:seats].count		
			(0..from).each do |pax|
				next if seats[pax].blank?
				seat_number = SeatNumber.where("seat_number = ? and user_id = ?",seats[pax], bus_map.user_id).first
				Ticket.create!(pnr: "PNR"+seats[pax], seat_number: seats[pax], seat_number_id: seat_number.id, first_name: params["name"][pax], email_id: params["email"][pax], phone_number: params["phone"][pax], booking_id: booking.id)
			end
			render :partial => "booking_confirmation"
		else
			render :partial => "booking_expired"
		end
	end
end
