class BusServicesController < ApplicationController
  before_action :set_bus_service, only: [:show, :edit, :update, :destroy]

  # GET /bus_services
  # GET /bus_services.json
  def index
    @bus_services = BusService.where("user_id=?", current_user.id)
    @cities = City.get_cities_hash
  end

  # GET /bus_services/1
  # GET /bus_services/1.json
  def show
  end

  # GET /bus_services/new
  def new
    @bus_service = BusService.new
    @cities = City.get_cities_array
  end

  # GET /bus_services/1/edit
  def edit
    @cities = City.get_cities_array
  end

  def sync_calendar
    user = current_user
    @gcal_code = user.gcal_code
    gcal = GCal.new(@gcal_code)
    begin
      event_lists = gcal.list_events(params["cal_id"], user.id)
      render plain: 1
    rescue => ex
      render plain: 2
    end
  end

  # POST /bus_services
  # POST /bus_services.json
  def create
    @bus_service = BusService.new(bus_service_params)

    respond_to do |format|
      if @bus_service.save
        format.html { redirect_to @bus_service, notice: 'Bus service was successfully created.' }
        format.json { render :show, status: :created, location: @bus_service }
      else
        format.html { render :new }
        format.json { render json: @bus_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bus_services/1
  # PATCH/PUT /bus_services/1.json
  def update
    respond_to do |format|
      if @bus_service.update(bus_service_params)
        format.html { redirect_to @bus_service, notice: 'Bus service was successfully updated.' }
        format.json { render :show, status: :ok, location: @bus_service }
      else
        format.html { render :edit }
        format.json { render json: @bus_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bus_services/1
  # DELETE /bus_services/1.json
  def destroy
    @bus_service.destroy
    respond_to do |format|
      format.html { redirect_to bus_services_url, notice: 'Bus service was successfully destroyed.' }
      format.json {k head :no_content }
    end
  end

  def service_status
    @bus_service = BusService.find params[:service_id]
    @user = User.find params[:user_id]
    status = params[:status].to_i == 1 ? 2 : 1
    @mapping = BusCalendarMapping.where("bus_service_id=?", params[:service_id]).first
    if !@user.gcal_code.blank? and !@mapping.blank?
      @gcal_code = @user.gcal_code
      gcal = GCal.new(@gcal_code)
      if status == 2
        mobile_no = AdminType::SMS_ALERT_NUMBERS
        mobile_no.each do |phone_number|
          guest_message = "Hi sir/madam Your Bus Ticket Booked Successfully "
          SmsOut.send_sms(@user.id,phone_number ,guest_message)
        end
        message = "Hi sir/madam Your Bus service ticket Booked successfully"
        SmsOut.send_sms(@user.id,@user.mobile_number ,message)
        result = gcal.create_new_event(@bus_service.journey_date, @mapping.google_calendar_id)
        @bus_service.update_attributes(status: status, event_id: result.id)
      else
        mobile_no = AdminType::SMS_ALERT_NUMBERS
        mobile_no.each do |phone_number|
          guest_message = "Hi sir/madam Your Bus Ticket Blocked Successfully "
          SmsOut.send_sms(@user.id,phone_number ,guest_message)
        end
        message = "Hi sir/madam Your Bus service ticket Blocked successfully"
        SmsOut.send_sms(@user.id,@user.mobile_number ,message)
        result = gcal.delete_event(@bus_service.event_id, @mapping.google_calendar_id)
        @bus_service.update_attributes(status: status, event_id: nil)
      end
    end
    render partial: "service_status"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bus_service
      @bus_service = BusService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bus_service_params
      params.require(:bus_service).permit(:name, :status, :user_id, :origin, :destination, :journey_date)
    end
end


