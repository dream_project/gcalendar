class HomeController < ApplicationController
	before_action :authenticate_user!
	protect_from_forgery
	def index
  end

  def google_auth
  	@gcal_code = current_user.gcal_code
    gcal = GCal.new(@gcal_code)
    @url = gcal.authorize
    if @url.class == Google::Auth::UserRefreshCredentials
      @calendar_list = gcal.calendar_list
      # @room_types = HotelPropertyBedRoomType.get_room_types_array(current_user.account_id)
      # @mappings = GoogleCalendarMapping.where("account_id=?", current_user.account_id)
      # @mappings_hash = {}
      # @mappings.each {|mapping|
        # @mappings_hash[mapping.hotel_property_bed_room_type_id] = mapping.google_calendar_id
      # }
    end
    # if @gcal_code.present?
      # gcal = gcalStart.new(@gcal_code )
      # cal_list = gcal.
    # end
    # render :partial => "google_auth"
  end

  def google_auth2
		current_user = User.find_by_id(params["id"])
  	gcal = GCal.new(params[:google_auth])
    @resp,@code = gcal.authorize
    if @code == 304
      flash[:error] = @resp
    else
      current_user.update_attributes(:gcal_code => params[:google_auth])
      flash[:notice] = "Successfully Authenticated"
    end
		respond_to do |format|
		  format.html {render :partial => "google_auth2"}
		  format.js 
		end
  end
end
